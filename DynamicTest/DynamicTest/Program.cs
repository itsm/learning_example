﻿using System.Dynamic;
using System.Text.RegularExpressions;

namespace DynamicTest
{
    internal class Program
    {
        static void Main(string[] args)
        {

            dynamic obj = new DynamicClass();
            obj.IsReadOnly = "fdsa";
            obj.fd = "aaaaaaa";
            obj["test"] = "dddddddddddd";
            var c= (string a, string b) =>
            {
                return "yyyyyyyyyyyyyyyyyyyyyyyy";
            };
            obj.t = c;
 
            foreach(var t in obj)
            {
                Console.WriteLine(t.Key + "=" + t.Value.ToString());
            }
        }
        string a(string t) {
            return "fffffff";
        }
    }
}