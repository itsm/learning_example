﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Model验证器.Models
{
    public class User
    {
        [Required]
        [StringLength(16)]
        public string UserName { get; set; }
        
        [Required]
        [StringLength(8,MinimumLength =8)]
        public string Password { get; set; }
    }
}
