﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.DependencyInjection;
using Volo.Abp.ObjectMapping;

namespace Acme.BookStore.Books
{
    public class BookToBookDtoMapper : IObjectMapper<Book, BookDto>, ISingletonDependency
    {
        public BookDto Map(Book source)
        {
            BookDto bookDto = new BookDto();
            return Map(source, bookDto);
        }

        public BookDto Map(Book source, BookDto destination)
        {
            destination.AuthorId=source.AuthorId;
            destination.Name = source.Name;
            destination.Price = source.Price;
            destination.PublishDate = source.PublishDate;
            destination.Type = source.Type;
            destination.Id = source.Id;
            destination.CreationTime = source.CreationTime;
            destination.CreatorId = source.CreatorId;
            destination.LastModificationTime = source.LastModificationTime;
            destination.LastModifierId = source.LastModifierId;

            return destination;
        }
    }
}
