﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.BookStore.Authors
{
    public class AuthorCreatedEvent
    {
        public Author Author { get; set; }
    }
}
