﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Emailing;
using Volo.Abp.Emailing.Smtp;

namespace Acme.BookStore.Web
{
    public class EmailSmtpConfiguration :  ISmtpEmailSenderConfiguration, IEmailSenderConfiguration
    {
        SmtpOption options;

        public EmailSmtpConfiguration(IOptions<SmtpOption> options)
        {
            this.options = options.Value;
        }

        public async Task<string> GetDefaultFromAddressAsync()
        {
            return options.DefaultFromAddress;
        }

        public async Task<string> GetDefaultFromDisplayNameAsync()
        {
            return options.DefaultFromDisplayName;
        }

        public async Task<string> GetDomainAsync()
        {
            return options.Domain;
        }

        public async Task<bool> GetEnableSslAsync()
        {
            return options.EnableSsl;
        }

        public async Task<string> GetHostAsync()
        {
            return options.Host;
        }

        public async Task<string> GetPasswordAsync()
        {
            return options.Password;
        }

        public async Task<int> GetPortAsync()
        {
            return options.Port;
        }

        public async Task<bool> GetUseDefaultCredentialsAsync()
        {
            return options.UseDefaultCredentials;
        }

        public async Task<string> GetUserNameAsync()
        {
            return options.UserName;
        }
    }
}
