﻿using Acme.BookStore.Authors;
using Acme.BookStore.Books;
using AutoMapper;
using static Acme.BookStore.Web.Pages.Authors.CreateModalModel;
using static Acme.BookStore.Web.Pages.Authors.EditModalModel;
using static Acme.BookStore.Web.Pages.Books.CreateModalModel;
using static Acme.BookStore.Web.Pages.Books.EditModalModel;

namespace Acme.BookStore.Web
{
    public class BookStoreWebAutoMapperProfile : Profile
    {
        public BookStoreWebAutoMapperProfile()
        {
            CreateMap<CreateAuthorViewModel, CreateAuthorDto>();
            CreateMap<AuthorDto, EditAuthorViewModel>();
            CreateMap<EditAuthorViewModel, UpdateAuthorDto>();
            CreateMap<CreateBookViewModel, CreateUpdateBookDto>();
            CreateMap<BookDto, EditBookViewModel>();
            CreateMap<EditBookViewModel,CreateUpdateBookDto >();
        }
    }
}
