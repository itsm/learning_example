﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Acme.BookStore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;
using Volo.Abp.Features;

namespace Acme.BookStore.Web.Pages.Books
{
    [RequiresFeature("BookStore.Books")]
    public class IndexModel : BookStorePageModel
    {
        public IndexModel()
        {
            
        }
        public void OnGet()
        {

        }
    }
}