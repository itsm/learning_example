const { merge } = require('webpack-merge');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const common = require('./webpack.config.common.js');

module.exports = merge(common, {
    mode:'production',
    //devtool: 'source-map',   //源码映射
    //devtool: 'inline-source-map',   //源码映射
    //以上两个源码映射测试下来不起作用
    plugins: [
        new UglifyJSPlugin()    //移除未使用到的脚本
    ]
 });