Vue.component('search-select',{
   data:function(){
       return  {
           IsExpand:false,
           IsSearch:false,
           SelectedItem:{
               Text:'',
               Value:''
           }
       };
   },
   props:{
       AllowSearch:Boolean,
       Items:Array,
       SeletedValue:{
           type:String,
           default:''
       }
   },
   watch:{
       'SelectedItem.Value':{
            handler:function(val,oldval){
                var item=this.findItem(val);
                var oldItem=this.findItem(oldval);                
                this.$emit('change',item,oldItem);
            }
       }
       
   },
   created:function(){
        var item=this.findItem(this.SeletedValue);
        this.SelectItem(item);
   },
   mounted:function(){

   },
   computed:{
       SearchItems:function(){
           if(this.IsSearch==false || this.SelectedItem.Text=='') return this.Items;
           var result=[];
           for(var i in this.Items){
               var item=this.Items[i];
               if(item.Text.indexOf(this.SelectedItem.Text)>=0){
                   result.push(item);
               }
           }
           return result;
       }
   },
   methods:{
       Search:function(){
          this.IsSearch=true;
       },
       search_focus:function(event){
          var txt=event.target;
          txt.select();
          this.DropDown(true);
       },
       search_blur:function(){
            setTimeout(() => {
                this.DropDown(false);
            }, 150);
           if(this.IsSearch==false) return;
           if(this.SearchItems.length==1){
               this.SelectItem(this.SearchItems[0]);
           }
           else{
              if(this.SelectedItem.Value!=''){
                var item=this.findItem(this.SelectedItem.Value);
                this.SelectItem(item);
              }
              else{
                this.SelectedItem.Text='';
              }
           } 
           this.IsSearch=false;
       },
       DropDown:function(expand){
           if(expand==true||expand==false) {
               this.IsExpand=expand;
           }
           else{
               this.IsExpand=!this.IsExpand;
           }
       },
       SelectItem:function(item){
            if(item==null) return;
            this.SelectedItem.Text=item.Text;
            this.SelectedItem.Value=item.Value;
            this.DropDown(false);
            this.IsSearch=false;
       },
       findItem:function(value){
            var item;
            for(var i in this.Items){
                if(this.Items[i].Value==value){
                    item=this.Items[i];
                    break;
                }
            }
            return item;
       }
   },
   template:`<div class="search-select-contains" :class="{'search-select-contains-onExpand':IsExpand}" >
   <input class="search-select-search" type="text" v-model="SelectedItem.Text" @focus="search_focus" @input="Search" @blur='search_blur' :readonly="!AllowSearch"
   /><span class="search-select-search-button" @click="DropDown">▼</span>
   <div class="search-select-items-contains" v-show="IsExpand"  >
       <div class="search-select-item" @click="SelectItem(item)" v-for="item in SearchItems">{{item.Text}}</div>
   </div>
</div>
   `
});