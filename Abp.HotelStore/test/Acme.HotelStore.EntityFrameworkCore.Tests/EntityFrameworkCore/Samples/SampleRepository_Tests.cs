﻿using Acme.HotelStore.Samples;

namespace Acme.HotelStore.EntityFrameworkCore.Samples
{
    public class SampleRepository_Tests : SampleRepository_Tests<HotelStoreEntityFrameworkCoreTestModule>
    {
        /* Don't write custom repository tests here, instead write to
         * the base class.
         * One exception can be some specific tests related to EF core.
         */
    }
}
