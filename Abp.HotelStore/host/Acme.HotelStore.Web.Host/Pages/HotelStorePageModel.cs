﻿using Acme.HotelStore.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Acme.HotelStore.Pages
{
    public abstract class HotelStorePageModel : AbpPageModel
    {
        protected HotelStorePageModel()
        {
            LocalizationResourceType = typeof(HotelStoreResource);
        }
    }
}