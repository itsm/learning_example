﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;

namespace Acme.HotelStore.Pages
{
    public class IndexModel : HotelStorePageModel
    {
        public void OnGet()
        {
            
        }

        public async Task OnPostLoginAsync()
        {
            await HttpContext.ChallengeAsync("oidc");
        }
    }
}