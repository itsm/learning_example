﻿using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared.Components;
using Volo.Abp.DependencyInjection;

namespace Acme.HotelStore
{
    [Dependency(ReplaceServices = true)]
    public class HotelStoreBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "HotelStore";
    }
}
