﻿using AutoMapper;

namespace Acme.HotelStore
{
    public class HotelStoreWebAutoMapperProfile : Profile
    {
        public HotelStoreWebAutoMapperProfile()
        {
            //Define your AutoMapper configuration here for the Web project.
        }
    }
}
