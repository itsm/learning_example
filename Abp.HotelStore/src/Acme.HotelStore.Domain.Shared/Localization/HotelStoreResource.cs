﻿using Volo.Abp.Localization;

namespace Acme.HotelStore.Localization
{
    [LocalizationResourceName("HotelStore")]
    public class HotelStoreResource
    {
        
    }
}
