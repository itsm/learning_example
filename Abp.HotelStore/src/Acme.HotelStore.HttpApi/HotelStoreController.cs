﻿using Acme.HotelStore.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace Acme.HotelStore
{
    public abstract class HotelStoreController : AbpController
    {
        protected HotelStoreController()
        {
            LocalizationResource = typeof(HotelStoreResource);
        }
    }
}
