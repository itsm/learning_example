﻿using Acme.HotelStore.Permissions;
using System.Threading.Tasks;
using Volo.Abp.UI.Navigation;

namespace Acme.HotelStore.Web.Menus
{
    public class HotelStoreMenuContributor : IMenuContributor
    {
        public async Task ConfigureMenuAsync(MenuConfigurationContext context)
        {
            if (context.Menu.Name == StandardMenus.Main)
            {
                await ConfigureMainMenu(context);
            }
        }

        private async Task ConfigureMainMenu(MenuConfigurationContext context)
        {
            var administrationMenu = context.Menu.GetAdministration();
            //Add main menu items.
            if (await context.IsGrantedAsync(HotelStorePermissions.Hotel.Default))
            {
                context.Menu.AddItem(
                    new ApplicationMenuItem("HotelStore", "酒店管理")
                    .AddItem(new ApplicationMenuItem("HotelStore.Hotel", "酒店管理", "/HotelStore"))
                    );
            }
        }
    }
}