﻿using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace Acme.HotelStore
{
    [DependsOn(
        typeof(AbpDddDomainModule),
        typeof(HotelStoreDomainSharedModule)
    )]
    public class HotelStoreDomainModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
  
        }
    }
}
