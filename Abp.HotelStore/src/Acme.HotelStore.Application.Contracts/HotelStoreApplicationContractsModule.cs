﻿using Volo.Abp.Application;
using Volo.Abp.Modularity;
using Volo.Abp.Authorization;

namespace Acme.HotelStore
{
    [DependsOn(
        typeof(HotelStoreDomainSharedModule),
        typeof(AbpDddApplicationContractsModule),
        typeof(AbpAuthorizationModule)
        )]
    public class HotelStoreApplicationContractsModule : AbpModule
    {

    }
}
