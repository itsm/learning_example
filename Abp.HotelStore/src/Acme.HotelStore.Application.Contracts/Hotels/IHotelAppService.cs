﻿using Acme.HotelStore.Hotels;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Acme.HotelStore.Hotels
{
    public interface IHotelAppService:ICrudAppService<HotelDto,Guid, PagedAndSortedResultRequestDto,CreateOrUpdateHotelDto>
    {
    }
}
