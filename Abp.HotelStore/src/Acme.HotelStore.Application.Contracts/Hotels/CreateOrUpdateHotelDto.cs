﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.HotelStore.Hotels
{
    public class CreateOrUpdateHotelDto
    {
        public string HotelName { get; set; }

        public string Address { get; set; }
    }
}
