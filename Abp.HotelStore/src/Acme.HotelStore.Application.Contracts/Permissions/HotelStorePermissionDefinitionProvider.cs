﻿using Acme.HotelStore.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace Acme.HotelStore.Permissions
{
    public class HotelStorePermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(HotelStorePermissions.GroupName, L("Permission:HotelStore"));
            var booksPermission = myGroup.AddPermission(HotelStorePermissions.Hotel.Default, L("Permission:Hotel"));
            booksPermission.AddChild(HotelStorePermissions.Hotel.Create, L("Permission:Hotel.Create"));
            booksPermission.AddChild(HotelStorePermissions.Hotel.Edit, L("Permission:Hotel.Edit"));
            booksPermission.AddChild(HotelStorePermissions.Hotel.Delete, L("Permission:Hotel.Delete"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<HotelStoreResource>(name);
        }
    }
}