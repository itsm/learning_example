﻿using JetBrains.Annotations;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Acme.HotelStore.EntityFrameworkCore
{
    public class HotelStoreModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
    {
        public HotelStoreModelBuilderConfigurationOptions(
            [NotNull] string tablePrefix = "",
            [CanBeNull] string schema = null)
            : base(
                tablePrefix,
                schema)
        {

        }
    }
}