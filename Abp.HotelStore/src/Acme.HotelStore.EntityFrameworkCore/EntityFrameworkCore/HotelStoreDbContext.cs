﻿using Acme.HotelStore.Hotels;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace Acme.HotelStore.EntityFrameworkCore
{
    [ConnectionStringName(HotelStoreDbProperties.ConnectionStringName)]
    public class HotelStoreDbContext : AbpDbContext<HotelStoreDbContext>, IHotelStoreDbContext
    {
        /* Add DbSet for each Aggregate Root here. Example:
         * public DbSet<Question> Questions { get; set; }
         */
        public DbSet<Hotel> Hotels { get; set; }

        public HotelStoreDbContext(DbContextOptions<HotelStoreDbContext> options) 
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ConfigureHotelStore();
        }
    }
}