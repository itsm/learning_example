﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Acme.HotelStore.EntityFrameworkCore
{
    [DependsOn(
        typeof(HotelStoreDomainModule),
        typeof(AbpEntityFrameworkCoreModule)
    )]
    public class HotelStoreEntityFrameworkCoreModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<HotelStoreDbContext>(options =>
            {
                /* Add custom repositories here. Example:
                 * options.AddRepository<Question, EfCoreQuestionRepository>();
                 */
                options.AddDefaultRepositories<HotelStoreDbContext>(true);
            });
        }
    }
}