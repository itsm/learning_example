﻿using AutoMapper;
using Acme.HotelStore.Hotels;

namespace Acme.HotelStore
{
    public class HotelStoreApplicationAutoMapperProfile : Profile
    {
        public HotelStoreApplicationAutoMapperProfile()
        {
            CreateMap<Hotel, HotelDto>();
            CreateMap<CreateOrUpdateHotelDto, Hotel>();
            CreateMap<HotelDto, CreateOrUpdateHotelDto>();
        }
    }
}