﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDAL
{
    public interface IGroupDAL
    {
         Model.GroupModel GetModel(string Code);

         int Add(Model.GroupModel model);

         int Add(IEnumerable<Model.GroupModel> models);

         int Update(Model.GroupModel model);

         int Update(IEnumerable<Model.GroupModel> models);

         int Delete(string Code);
    }
}
