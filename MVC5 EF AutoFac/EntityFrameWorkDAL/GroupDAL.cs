﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace EntityFrameWorkDAL
{
    public class GroupDAL : Repository<Model.GroupModel>, IDAL.IGroupDAL
    {

        public GroupDAL() : base(new DBContext())
        {

        }

        public GroupModel GetModel(string Code)
        {
            var model = Search(r => r.Code == Code).FirstOrDefault();
            return model;
        }

        public int Delete(string Code)
        {
            return Delete(r => r.Code == Code);
        }
    }
}
