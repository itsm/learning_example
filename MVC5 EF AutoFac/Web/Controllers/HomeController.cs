﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        IBLL.IManagerBLL manager = null;
        public HomeController(IBLL.IManagerBLL manager)
        {
            this.manager = manager;
        }
        public ActionResult Index()
        {
            var model = manager.GetModel("test");
            if (model == null)
            {
                model = new Model.ManagerModel()
                {
                    UserName = "test",
                    RealName = "测试人员"

                };
                manager.Add(model);
            }
            manager.Update(model);
            manager.Update(new Model.ManagerModel() { UserName = "test", RealName = "测试修改" });

            Response.Write(model.RealName);
            model = manager.GetModel("test");
            Response.Write(model.RealName);
            return View();
        }
    }
}