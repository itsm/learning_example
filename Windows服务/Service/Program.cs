﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Service
{
    class Program
    {
        /// <summary>
        /// 当前程序名 +（install 安装服务，uninstall 停止服务 start 启动服务 stop 停止服务）
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            HostFactory.Run(r =>
            {
                r.Service<Service>();
                r.RunAsLocalSystem();
                r.SetServiceName("测试");
                r.SetDisplayName("我的测试");
                r.SetDescription("fdfdfdfdf");
            });
        }
    }
}
