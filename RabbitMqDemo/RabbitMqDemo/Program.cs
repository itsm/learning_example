﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RabbitMqDemo.HosteServices;

namespace RabbitMqDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var hostBuilder = Host.CreateDefaultBuilder(args);
            hostBuilder.ConfigureServices(services =>
            {
                services.AddHostedService<RabbitMqProduct>();
                services.AddHostedService<RabbitMqConsumer>();
            });
            var host = hostBuilder.Build();
            host.Start();
            host.WaitForShutdown();
        }
    }
}