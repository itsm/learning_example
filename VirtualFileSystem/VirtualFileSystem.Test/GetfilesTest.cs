using Microsoft.Extensions.FileProviders;
using System.Runtime.CompilerServices;

namespace VirtualFileSystem.Test
{
    [TestClass]
    public class GetfilesTest
    {
        [TestMethod]
        public void FileTest()
        {
            var virtualFileProvider = new DictionaryEmbeddedFileProvider(this.GetType().Assembly);
            var file = virtualFileProvider.GetFileInfo("/TestData/json.json");
            Assert.AreNotEqual(file.GetType(), typeof(NotFoundFileInfo));
            Assert.AreEqual(file.Name, "json.json");
        }

        [TestMethod]
        public void DirectoryTest()
        {
            var virtualFileProvider = new DictionaryEmbeddedFileProvider(Assembly.GetExecutingAssembly());
            var files = virtualFileProvider.GetDirectoryContents("/");
            Assert.AreEqual(files.First().Name, "TestData");
        }

        [TestMethod]
        public void DirectoryByRequestPathTest()
        {
            var virtualFileProvider = new DictionaryEmbeddedFileProvider(Assembly.GetExecutingAssembly(), "DirectoryByRequestPathTest/Main");
            var files = virtualFileProvider.GetDirectoryContents("/");
            Assert.AreEqual(files.First().Name, "DirectoryByRequestPathTest");
            files = virtualFileProvider.GetDirectoryContents("/DirectoryByRequestPathTest/Main");
            Assert.AreEqual(files.First().Name, "TestData");
        }

        [TestMethod]
        public void ReqPathTest()
        {
            var virtualFileProvider = new DictionaryEmbeddedFileProvider(this.GetType().Assembly, "ReqPath");
            var fileInfo = virtualFileProvider.GetFileInfo("/ReqPath/TestData/json.json"); 
            Assert.AreEqual(fileInfo.Name, "json.json");
        }
    }
}