using Microsoft.Extensions.FileProviders;
using System.Diagnostics;

namespace VirtualFileSystem.Test
{
    [TestClass]
    public class PerformanceTest
    {
        /// <summary>
        /// 性能测试,相比系统自带EmbeddedFileProvider提升80%以上性能
        /// </summary>
        [TestMethod]
        public void EmbeddedPerformance80Test()
        {
            var virtualFileProvider = new DictionaryEmbeddedFileProvider(this.GetType().Assembly);
            System.Diagnostics.Stopwatch stopwatch = new();
            stopwatch.Start();
            var testNumber = 1000000;
            for (var i = 0; i < testNumber; i++)
            {
                var file = virtualFileProvider.GetFileInfo("/TestData/json.json");
            }
            stopwatch.Stop();
            var virtualFileProviderTime = stopwatch.ElapsedMilliseconds;

            var embeddedFileProvider = new EmbeddedFileProvider(this.GetType().Assembly);
            stopwatch.Restart();
            for (var i = 0; i < testNumber; i++)
            {
                var file = embeddedFileProvider.GetFileInfo("/TestData/json.json");
            }
            stopwatch.Stop();

            var embeddedFileProviderTime = stopwatch.ElapsedMilliseconds;

            var perNumber = (embeddedFileProviderTime - virtualFileProviderTime) * 1.0 / virtualFileProviderTime;
            Debug.WriteLine($"embeddedFileProviderTime:{embeddedFileProviderTime},virtualFileProviderTime:{virtualFileProviderTime},perNumber:{perNumber}");          
            Assert.AreEqual(perNumber > 3, true);
        }
    }
}