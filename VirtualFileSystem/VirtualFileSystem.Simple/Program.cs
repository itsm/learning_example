using System.Reflection;
using System.Runtime.CompilerServices;
using VirtualFileSystem.Test;

namespace VirtualFileSystem.Simple
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddRazorPages();
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseVirtualFileSystem(option =>
            {
                option.FileProviders.AddEmbeddedFile<Program>("VirtualFile")
                                    .AddEmbeddedFile(typeof(PerformanceTest).Assembly,"")
                                    .AddPhysicalFile(builder.Environment.WebRootPath);
                if (builder.Environment.IsDevelopment())
                {
                    option.EnableDirectoryBrowser = true;
                    option.FileProviders.ReplaceAllEmbedToPhysical(builder.Environment.ContentRootPath);
                }
            });
        

            app.UseRouting();

            app.UseAuthorization();

            app.MapRazorPages();

            app.Run();
        }
    }
}