﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace HostOptions
{
    public class Program
    {
        static  void Main(string[] args)
        {
            var hostBuild = new HostBuilder()
                .ConfigureDefaults(args) //此处会将appsettings.json配置文件增加进来
                .ConfigureHostConfiguration(configurationBuilder =>  //最先执行的基础配置，ConfigureAppConfiguration或程序设置的都会覆盖此配置
                {

                })
                .ConfigureAppConfiguration(configurationBuilder =>
                {
                    configurationBuilder.AddJsonFile("config.json", true, true); //增加config.json为可选配置文件，且监控文件内容变化
                })
                .ConfigureServices((hostBuilderContext, services) =>
                {
                    services.Configure<ModelOptions>(hostBuilderContext.Configuration.GetSection("ModelOptions")); //绑定配置文件中的ModelOptions节点，
                                                                                                                   //多个配置文件都有该节点时会按加载的先后顺序自动合并
                    services.Configure<ModelOptions>(options =>
                    {
                       // options.UserName = "Program_Admin";
                        options.Mobile = "184444";
                    });
                })
                .ConfigureServices(ConfigureServices);
            var host = hostBuild.Build();
            host.Run();
        }

        public static void ConfigureServices(HostBuilderContext hostBuilderContext, IServiceCollection services)
        {
            services.AddScoped<IocClass>();
            services.AddHostedService<BackgroudService>();

        }
    }
}