﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HostOptions
{
    public class IocClass
    {
        ModelOptions options;
        ModelOptions optionsSnapshot;
        ModelOptions optionsMonitor;

        public IocClass(IOptions<ModelOptions> options, //整个应用程序生存周期共用一个缓存，Services.Configure中的方法只执行一次
            IOptionsSnapshot<ModelOptions> optionsSnapshot,  //注入时创建新选项实例并重新执行Services.Configure中的方法获取最新配置
            IOptionsMonitor<ModelOptions> optionsMonitor)  //每次调用CurrentValue时实时调用Services.Configure中的方法获取最新配置
        {
            this.options = options.Value; 
            this.optionsSnapshot = optionsSnapshot.Value;
            this.optionsMonitor = optionsMonitor.CurrentValue;
        }

        public void TestMothed()
        {
            Console.WriteLine("options:" + System.Text.Json.JsonSerializer.Serialize(options));
            Console.WriteLine("optionsSnapshot:" + System.Text.Json.JsonSerializer.Serialize(optionsSnapshot));
            Console.WriteLine("optionsMonitor:" + System.Text.Json.JsonSerializer.Serialize(optionsMonitor));
            Console.WriteLine();
        }
    }
}
