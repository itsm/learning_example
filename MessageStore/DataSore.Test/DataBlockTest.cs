﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DataStore.Test
{
    [TestClass]
    public class DataBlockTest
    {
        [TestMethod]
        public void WriteAndReadTest()
        {
            var dataStream = new MemoryStream();
            var IndexStream = new MemoryStream();
            var block = new DataBlock(dataStream, IndexStream, 4096);
            Span<byte> data = Encoding.UTF8.GetBytes("hello world");
            for (var i = 0; i < 10; i++)
            {
                block.WriteData(data);
            }
            Assert.AreEqual(block.DataCount, 10);
            var testData = block.ReadData(0);
            Assert.IsTrue(data == testData);
            testData = block.ReadData(10);
            Assert.IsTrue(data == testData);
            testData = block.ReadData(5);
            Assert.IsTrue(data == testData);

            block.Flush();
            block.Dispose();
        }
    }
}
