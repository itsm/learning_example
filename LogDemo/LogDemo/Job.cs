﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading;

namespace LogDemo
{
    public class Job : BackgroundService
    {
        private readonly ILogger logger;
        public Job(ILogger<Job> logger)
        {
            this.logger = logger;
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var task= Task.Run(() =>
            {
                int count = 0;
                while (stoppingToken.IsCancellationRequested == false)
                {
                    count++;
                    logger.LogInformation($"working,execution count:{count}");
                    Task.Delay(1000).Wait();
                }
            }, stoppingToken);
            return task;
        }
    }
}
