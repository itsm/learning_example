﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LogDemo
{
    public class MyLogInfo
    {
        public MyLogInfo(LogLevel logLevel, string category, EventId eventId,  string logMessage, Exception? exception)
        {
            LogLevel = logLevel;
            Category = category;
            EventId = eventId;
            LogMessage = logMessage;
            Exception = exception;
            Date= DateTime.Now;
        }

        public LogLevel LogLevel { get; set; }

        public string Category { get; set; }

        public string LogMessage { get; set; }

        public EventId EventId { get; set; }

        public Exception? Exception { get; set; }

        public DateTime Date { get; set; }

    }
}
