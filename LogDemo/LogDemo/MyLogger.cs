﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LogDemo
{
    public class MyLogger : ILogger
    {
        private string categoryName;
        private ChannelWriter<MyLogInfo> channelWriter;

        public MyLogger(string categoryName,ChannelWriter<MyLogInfo> channelWriter)
        {
            this.categoryName = categoryName;
            this.channelWriter = channelWriter; 

        }
        public IDisposable? BeginScope<TState>(TState state) where TState : notnull
        {
            return null;
        }


        public bool IsEnabled(LogLevel logLevel)
        {
            return logLevel != LogLevel.None;
        }

        void ILogger.Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter)
        {
            
            if (IsEnabled(logLevel) == false) return;            
            var msg = formatter(state, exception);
            var logInfo = new MyLogInfo(logLevel, categoryName, eventId, msg, exception);
            channelWriter.WriteAsync(logInfo).GetAwaiter().GetResult();
        }


    }
}
