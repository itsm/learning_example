﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EFCore.Models
{
    public class Role
    {
        [Key]
        public string RoleID { get; set; }

        public string RoleName { get; set; }

        public List<User> Users { get; set; }

    }
}
