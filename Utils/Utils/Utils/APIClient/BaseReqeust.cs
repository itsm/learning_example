﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utils.APIClient
{
    /// <summary>
    /// 请求基类
    /// </summary>
    public class BaseReqeust<T> where T : BaseResponseData, new()
    {
        public  BaseResponse<T> GetResponse()
        {
            return new BaseResponse<T>();
        }
    }
}