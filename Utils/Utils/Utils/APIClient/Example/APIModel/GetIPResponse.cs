﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.APIClient;

namespace Utils.APIClient.Example.APIModel
{
    public class GetIPResponse : BaseResponseData
    {
        public int StateCode { get; set; }
        public string Reason { get; set; }
        public ResultModel Result { get; set; }

        public class ResultModel
        {
            public string IP { get; set; }
            public string Country { get; set; }
            public string Province { get; set; }
            public string City { get; set; }
            public object District { get; set; }
            public string Isp { get; set; }
        }

    }
}
