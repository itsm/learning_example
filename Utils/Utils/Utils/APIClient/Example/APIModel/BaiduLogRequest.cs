﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.APIClient;

namespace Utils.APIClient.Example.APIModel
{
    [RemoteService("/img/{img}","GET")]
    public class BaiduLogRequest: BaseReqeust<FileItem>
    {
        [JsonIgnore]
        public string img { get; set; }
    }
}
