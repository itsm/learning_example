﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.APIClient;

namespace Utils.APIClient.Example.APIModel
{
    [RemoteService("/s?wd={wd}","GET")]
    public class BaiduRequest:BaseReqeust<FileItem>
    {
        [JsonIgnore]
        public string wd { get; set; }
    }
}
