﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.APIClient;

namespace Utils.APIClient.Example.APIModel
{
    [RemoteService("single/ip?key={key}&ip={ip}", "GET")]
    public class GetIPRequest: BaseReqeust<GetIPResponse>
    {
        [JsonIgnore]
        public string key { get; set; }

        [JsonIgnore]
        public string ip { get; set; }
    }
}
