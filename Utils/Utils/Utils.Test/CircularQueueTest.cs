using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Specialized;

namespace Utils.Test
{
    [TestClass]
    public class CircularQueueTest
    {
        [TestMethod]
        public void EnqueueAndDequeueTest()
        {
            var queue = new CircularQueue<int>(10);
            for (var i = 0; i < 10; i++)
            {
                queue.Enqueue(i);
                Assert.AreEqual<int>(i + 1, queue.Count);
            }
            queue.Enqueue(10);
            Assert.AreEqual<int>(10, queue.Count);
            var t = queue.Dequeue();
            Assert.AreEqual<int>(1, t);
            Assert.AreEqual<int>(9, queue.Count);

            for (var i = 0; i < 9; i++)
            {
                t = queue.Dequeue();
                Assert.AreEqual<int>(i + 2, t);
            }
            Assert.AreEqual<int>(0, queue.Count);
        }

        [TestMethod]
        public void FullAndHalfTest()
        {
            var queue = new CircularQueue<int>(10);
            for (var i = 0; i < 19; i++)
            {
                queue.Enqueue(i);
            }
            for (var i = 0; i < 4; i++)
            {
                var t = queue.Dequeue();
                Assert.AreEqual<int>(9 + i, t);
            }
            Assert.AreEqual<int>(6, queue.Count);

            int index = 0;
            foreach (var t in queue)
            {
                Assert.AreEqual<int>(13 + index, t);
                index++;
            }
        }

        [TestMethod]
        public void CopyToTest()
        {
            var queue = new CircularQueue<int>(10);
            for (var i = 0; i < 15; i++)
            {
                queue.Enqueue(i);
            }
            var datas = new int[10];
            queue.CopyTo(datas, 0);
            for(var i = 0; i < datas.Length; i++)
            {
                Assert.AreEqual<int>(datas[i], 5 + i);
            }
        }
    }
}
